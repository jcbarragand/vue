const vm = new Vue({
    el: 'main',
    data:{
        mensaje: 'Hola Mundo ',
        nuevaTarea : null,
        juegos:[
            {
                titulo: 'aprende vue',
                genero: true,
                antiguedad: 23 
            },
            {
                titulo: 'aprende es6',
                prioridad: false,
                antiguedad: 135 
            },
            {
                titulo: 'publica',
                prioridad: true,
                antiguedad: 378 
            }
        ]
    },
    methods:{
        agregarTarea(){
            this.tareas.unshift({
                titulo: this.nuevaTarea,
                prioridad:false,
                antiguedad:1
            });
            this.nuevaTarea = null;
        }
    },
    computed:{
        mensajeAlReves(){
            return this.mensaje.split('').reverse().join('');
        },
        tareasConPrioridad(){
            return this.tareas.filter((tarea) => tarea.prioridad);
        },
        tareasPorAntiguedad(){
            return this.tareas.sort((a,b ) => b.antiguedad - a.antiguedad);
        }
    }
});