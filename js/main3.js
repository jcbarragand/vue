Vue.filter('alReves', (valor) => valor.split('').reverse().join(''));

const vm = new Vue({
    el: 'main',
    data:{
        busqueda:'',
        minimo : 5,
        juegos:[
            {
                titulo: 'Battle',
                genero: 'FPS',
                puntuacion: 9 
            },
            {
                titulo: 'Civil',
                genero: 'Estrategia',
                puntuacion: 10 
            },
            {
                titulo: 'Resident',
                genero: 'Terror',
                puntuacion: 8 
            }
        ]
    },
    computed:{
        mejoresJuegos(){
            return this.juegos.filter((juego) => juego.puntuacion >= this.minimo);
        },
        buscarJuego(){
            return this.juegos.filter((juego) => juego.titulo.includes(this.busqueda));
        }
    }
});